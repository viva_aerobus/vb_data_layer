export const airportsSelector = airportsArray => {
  return airportsArray.map((item, index) => ({
    label: item.name,
    value: item.code,
    index,
    country: item.countryCode
  }));
};

export const currenciesSelector = currenciesArray => {
  return currenciesArray.map((item, index) => ({
    label: item.code,
    value: '',
    index
  }));
};
