import { AsyncStorage } from 'react-native';

const createHeaders = async () => {
  const token = await AsyncStorage.getItem('x-token');
  const searchFlights = await AsyncStorage.getItem('searchFlights');
  const parsedSearchFlights = JSON.parse(searchFlights);
  const apiHeaders = {
    'x-useridentifier': 'q3TSS2lKKC7tYQd8VNf3J6AEBvVhcs',
    'x-token': token,
    'x-externalrateid': 'J6JYKMJUEV7FU6M',
    authority: 'api-test-vivaair.ezyflight.se',
    'x-bookingcurrencycode': parsedSearchFlights.currencyCode
  };
  return apiHeaders;
};

export default createHeaders;
