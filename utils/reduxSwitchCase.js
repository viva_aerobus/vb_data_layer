const reduxSwitchCase = (cases, key) =>
  key in cases ? cases[key] : state => state;

export default reduxSwitchCase;
