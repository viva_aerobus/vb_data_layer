import axios from 'axios';
import { AsyncStorage } from 'react-native';
import endpoints from './ezyEndpoints';
import apiHeaders from '../utils/credentials';

export async function getTripDataService() {
  const tripData = await AsyncStorage.getItem('tripData');
  const parsedTripData = JSON.parse(tripData);
  const serviceHeaders = await apiHeaders();
  const data = {};
  try {
    await axios({
      method: 'post',
      url: endpoints.createTrip,
      data: parsedTripData,
      headers: serviceHeaders
    })
      .then(res => {
        data.createTripArray = res.data;
      })
      .catch(err => {
        data.createTripArray = err.dotRezErrorType;
      });
    return {
      status: 'success',
      code: 'get_trip_data_success',
      data
    };
  } catch (error) {
    return {
      status: 'error',
      code: 'get_trip_data_error',
      data: {}
    };
  }
}
