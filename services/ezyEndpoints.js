const endpoints = {
  airports:
    'https://api-test-vivaair.ezyflight.se/api/v1/Availability/Airports',
  config: 'https://api-test-vivaair.ezyflight.se/api/v1/Resources/Config',
  currencies:
    'https://api-test-vivaair.ezyflight.se/api/v1/Availability/Currencies',
  prepare: 'https://api-test-vivaair.ezyflight.se/api/v1/Availability/Prepare',
  search: 'https://api-test-vivaair.ezyflight.se/api/v1/Availability/Search',
  lowFares:
    'https://api-test-vivaair.ezyflight.se/api/v1/Availability/LowFares',
  createTrip: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/CreateTrip',
  services: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/Services',
  getSeats: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/GetSeats',
  addSeat: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/AddSeats',
  insurances: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/Insurances',
  postPassengers:
    'https://api-test-vivaair.ezyflight.se/api/v1/Booking/PostPassengers',
  paymentAvailable:
    'https://api-test-vivaair.ezyflight.se/api/v1/Payment/Available',
  ssrChange: 'https://api-test-vivaair.ezyflight.se/api/v1/Booking/SsrChange',
  sessionKeepAlive:
    'https://api-test-vivaair.ezyflight.se/api/v1/Resources/SessionKeepAlive'
};

export default endpoints;
