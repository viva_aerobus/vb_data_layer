import axios from 'axios';
import { AsyncStorage } from 'react-native';
import endpoints from './ezyEndpoints';

export async function getFlightsDataService() {
  const searchFlights = await AsyncStorage.getItem('searchFlights');
  const parsedFlights = JSON.parse(searchFlights);
  const data = {};
  try {
    await axios({
      method: 'post',
      url: endpoints.search,
      data: parsedFlights
    })
      .then(async res => {
        data.searchResponse = res.data.routes;
        await AsyncStorage.setItem('x-token', res.headers['x-token']);
      })
      .catch(err => {
        data.searchResponse = 'noFlights';
        console.error(err);
      });
    return {
      status: 'success',
      code: 'get_flights_data_success',
      data
    };
  } catch (error) {
    return {
      status: 'error',
      code: 'get_flights_data_error',
      data: {}
    };
  }
}
