import axios from 'axios';
import endpoints from './ezyEndpoints';

export async function getFormDataService() {
  const data = {};
  try {
    await axios.all([
        axios.get(endpoints.airports),
        axios.get(endpoints.config),
        axios.get(endpoints.currencies)
      ])
      .then(
        axios.spread((airports, config, currencies) => {
          data.airportsArray = airports.data.airports;
          data.configArray = config.data;
          data.currenciesArray = currencies.data.currencies;
        })
      );
    return {
      status: 'success',
      code: 'get_form_data_success',
      data
    };
  } catch (error) {
    console.log('error in service: ', error);
    return {
      status: 'error',
      code: 'get_form_data_failure',
      data: {}
    };
  }
}
