import { GET_FORM_DATA_ATTEMPT, GET_FORM_DATA_RESPONSE } from './actionTypes';

import { getFormDataService } from '../services/formData';

export const getFormData = () => ({
  types: [GET_FORM_DATA_ATTEMPT, GET_FORM_DATA_RESPONSE],
  callAPI: () => getFormDataService()
});
