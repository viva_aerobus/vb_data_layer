import { GET_TRIP_DATA_ATTEMPT, GET_TRIP_DATA_RESPONSE } from './actionTypes';

import { getTripDataService } from '../services/tripData';

export const getTripData = () => ({
  types: [GET_TRIP_DATA_ATTEMPT, GET_TRIP_DATA_RESPONSE],
  callAPI: () => getTripDataService()
});
