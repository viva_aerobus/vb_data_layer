import {
  GET_FLIGHTS_DATA_ATTEMPT,
  GET_FLIGHTS_DATA_RESPONSE
} from './actionTypes';

import { getFlightsDataService } from '../services/flightsData';

export const getFlightsData = () => ({
  types: [GET_FLIGHTS_DATA_ATTEMPT, GET_FLIGHTS_DATA_RESPONSE],
  callAPI: () => getFlightsDataService()
});
