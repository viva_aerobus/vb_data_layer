import reduxSwitchcase from '../../utils/reduxSwitchCase';
import handlers from './handlers';
import initialState from './initialState';

export default function auth(state = initialState, action) {
  return reduxSwitchcase(handlers, action.type)(state, action, initialState);
}
