export default (state, { response }) => {
  const { status, data, code } = response;
  return {
    ...state,
    process: null,
    status,
    code,
    airportsArray: data.airportsArray,
    configArray: data.configArray,
    currenciesArray: data.currenciesArray
  };
};
