const initialState = {
  process: null,
  status: null,
  code: null,
  searchResponse: []
};

export default initialState;
