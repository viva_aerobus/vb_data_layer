export default (state, { response }) => {
  const { status, data, code } = response;
  return {
    ...state,
    process: null,
    status,
    code,
    createTripArray: data.createTripArray
  };
};
