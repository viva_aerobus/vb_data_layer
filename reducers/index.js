import { combineReducers } from 'redux';
import formData from './formData';
import flightsData from './flightsData';
import tripData from './tripData';

export default combineReducers({
  formData,
  flightsData,
  tripData
});
