export const darkBlue = 'rgba(40, 57, 133, 1.0)';
export const lightBlue = 'rgba(94, 122, 242, 1.0)';
export const darkGray = 'rgba(94, 94, 94, 1.0)';
export const divisionBarGray = 'rgba(194, 194, 234, 1.0)';
export const modalButtonGray = 'rgba(159, 159, 189, 1.0)';
export const orangeText = 'rgba(227, 125, 70, 1.0)';
export const mediumGray = 'rgba(171, 183, 201, 1.0)';

export const listingsGradientSteps = [
  'rgba(237,244,250,1)',
  'rgba(189,201,206,1)'
];

export const vbGreenActive = 'rgba(96,172,4,1)';
export const vbGreenShaded = 'rgba(122,181,76,1)';
export const vbGreenText = 'rgba(79,144,2,1)';
export const vbDarkGreenText = 'rgba(52,94,0,1)';
export const vbGreenModalHeader = 'rgba(102,161,3,1)';
export const vbLightGreenText = 'rgba(160,255,0,1)';
export const vbMediumGray = 'rgba(153,153,153,1)';
export const vbLightGray = 'rgba(238,238,238,1)';
export const vbLightBackground = 'rgba(236,236,236,1)';
export const vbItemTextGray = 'rgba(200,200,200,1)';
export const vbFareItem1 = 'rgba(90,141,3,1)';
export const vbFareItem2 = 'rgba(63,100,0,1)';
